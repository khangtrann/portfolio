const $window = $(window);
const $carousel = $("#carousel");
const $projectTimeline = $(".timeline");
const $portfolioModal = $("#portfolioModal");

const portfolioModalContent = `
  <object data="./TranMinhKhang_CV.pdf" frameborder="0" width="100%" style="height: 85vh;">
      <embed src="./TranMinhKhang_CV.pdf" frameborder="0" width="100%" style="height: 85vh;" frameborder="0">
  </object>
`;

const defaultProjectTimelineTemplate = `
  <li class="timeline-event">
    <label class="timeline-event-icon"></label>
    <div class="timeline-event-copy">
        <p class="timeline-event-thumbnail timeline__time-range"></p>
        <h3 class="timeline__project-title mb-4"></h3>
        <div class="description font-ram">
            <div class="font-size-20">
                <div class="row mb-3">
                    <div class="col-md-6">
                      <span class="fas fa-file-medical-alt"></span>
                      &nbsp; Description
                    </div>
                    <div class="col-md-6" v-if="show">
                      <span class="fas fa-users"></span>
                      &nbsp; Members - 
                      <span class="timeline__project-members text-black-50"></span>
                    </div>
                </div>
            </div>
            <div class="ml-3 text-black-50">
              <p class="m-0 timeline__project-description" style="line-height: 20px;"></p>
            </div>
        </div>
        <div class="technology">
            <div class="row">
                <div class="timeline__project-technologies col-md-6 font-ram">
                    <div class="font-size-20">
                        <span class="fas fa-microchip"></span>
                        &nbsp; Technology
                    </div>
                </div>
                <div class="timeline__project-responsibilities col-md-6 font-ram">
                    <div class="font-size-20">
                        <span class="fas fa-user-shield"></span>
                        &nbsp; Responsibility
                    </div>
                </div>
            </div>
        </div>
        <div class="demo mt-3">
            <div class="row">
                <div class="timeline__project-related d-none col font-ram">
                    <div class="font-size-20">
                        <span class="fas fa-paperclip"></span>
                        &nbsp; Related
                    </div>
                </div>
            </div>
        </div>
    </div>
  </li>
`;

const projectsData = [
  {
    title: "Full-time Developer",
    timeRange: "October 2020 - Present",
    description:
      "Working with outsourcing & product projects at Can Tho's FPT Software",
    members: "7 people",
    technologies: [
      "ReactJS, AngularJS, Angular",
      "NodeJS, AWS Lambda",
      "Java, Spring Boot",
      "MongoDB Database",
    ],
    responsibilities: ["Full-stack developer"],
  },
  {
    title: "TMA Solutions Intern: Full-stack Java Web",
    timeRange: "June 2020 - September 2020",
    description:
      "Working on a web application supports tester in doing test case statistic, report with dashboard and useful filter",
    members: "10+ people",
    technologies: ["Angular", "Java, Spring Boot", "JPA, Maven, Kafka"],
    responsibilities: [
      "Implement REST API endpoint for new features",
      "Improve Frontend UI - UX",
      "Improved project configuration for better performance",
      "Fix bugs and implement enhancements that significantly improved web functionally and speed",
    ],
  },
  {
    title: "Freelance: Ecommerce website for Foody",
    timeRange: "February 2021 - Present",
    description: "Build an ecommerce website for food delivery",
    members: "1 people",
    technologies: [
      "ReactJS, Redux",
      "NestJS, Postgres Database",
      "Gitlab CI/CD",
    ],
    responsibilities: ["Full-stack developer"],
    related: {
      demo: "https://delikitchen.com.vn",
    },
  },
  {
    title:
      "Scientific Research: Apply Computer Vision on Raspberry Pi for create automatic camera",
    timeRange: "April 2019 - December 2019",
    description:
      "Apply Computer Vision on Raspberry Pi for detecting and tracking people.",
    members: "5 people",
    technologies: [
      "Python",
      "Deep Neural Networks (DNN) module",
      "OpenCV Library, Dlib's face detector",
      "GPIO.pi module for controlling and rotating servos",
      "ImageZMQ for streaming live video from Raspberry Pi to server",
    ],
    responsibilities: [
      "Setting up Raspberry Pi",
      "Design and assemble model for connecting Raspberry Pi, camera and servos",
      "Create module for detecting and tracking",
      "Create module for controlling and rotating servos",
    ],
    related: {
      github: "https://gitlab.com/khang_tran/nckh",
      demo: "https://youtu.be/5yhcqrR-CDs",
    },
  },
  {
    title: "Security Camera System",
    timeRange: "September 2020 - January 2021",
    description:
      "Develop a security camera system for detecting anonymous and push notification",
    members: "2 people",
    technologies: [
      "Python, OpenCV",
      "React Native",
      "Firebase Push Notification",
      "MQTT - Message Queuing Telemetry Transport",
      "HTTP Live Streaming",
    ],
    responsibilities: ["Mobile application development", "Backend development"],
    related: {
      github: "https://gitlab.com/khangtrann/security-camera-bare-workflow",
      demo: "https://drive.google.com/file/d/1I9Hm_iVw7AmLr3GOFOjMgBxzep7OOfnP/view?usp=sharing",
    },
  },
  {
    title: "Agricultural Cooperatives 4.0",
    timeRange: "December 2019 - May 2020",
    description: `
      <p style="margin-bottom: -2px !important;">Develop a system for agricultural production managing and diary logging.</p>
      <p style="margin-bottom: -2px !important;">Web application for managing all facilities such as plant production product, fertilizer and medical instrument.</p>
      <p style="margin-bottom: -2px !important;">Mobile application for logging all production diary of farmers.</p>
    `,
    members: "8 people",
    technologies: ["NodeJS, ExpressJS", "ReactJS", "MongoDB Database"],
    responsibilities: ["Database analysis & design", "Backend development"],
    related: {
      github: "https://github.com/tranminhkhang8198/HTX",
    },
  },
  {
    title: "Image Captioning",
    timeRange: "September 2019 - December 2019",
    description: `
      <p style="margin-bottom: -2px !important;">Build model for predicting image caption with Vietnamese language.</p>
      <p style="margin-bottom: -2px !important;">Setting up image captioning model on server to predict caption for image from Raspberry Pi Camera.</p>
      <p style="margin-bottom: -2px !important;">Send caption back from server to Raspberry Pi in order to speak out loud by speaker.</p>
    `,
    members: "5 people",
    technologies: [
      "Python",
      "Word Embedding",
      "Convolutional neural network (CNN)",
      "Recurrent neural network (RNN)",
      "ImageZMQ for sending image from Raspberry Pi Camera to server",
    ],
    responsibilities: [
      "Gather and pre-processing dataset",
      "Training model for predict caption",
      "Setting up environment for connection between server and Raspberry Pi.",
    ],
    related: {
      github: "https://github.com/tranminhkhang8198/NIEN-LUAN-2019",
      demo: "https://drive.google.com/file/d/1C-r2gWfg-GX7iFB7O4vLwqpGB_G5vpjs/view?usp=sharing",
    },
  },
  {
    title: "Freelance: Sport Live Streaming Web Application",
    timeRange: "September 2019 - November 2019",
    description:
      "Develop sport live streaming web application using HTTP Live Streaming FFMPEG",
    members: "3 people",
    technologies: [
      "NGINX - RTMP module, FFMPEG",
      "NodeJS, ExpressJS",
      "MongoDB & Mongoose",
    ],
    responsibilities: ["Database analysis & design", "Backend development"],
    related: {
      github: "https://github.com/tranminhkhang8198/streaming-website",
    },
  },
];

$(document).ready(function (e) {
  const { hash } = window.location;

  const isMobile = mobileCheck();
  if (isMobile) {
    $("header").addClass("d-none");
    $("#carousel").addClass("d-none");
    alert("Please use laptop or pc for better experience.");
    return;
  }

  if (["#portfolio", "#cv", "#download"].includes(hash)) {
    $portfolioModal.modal("show");
  }

  $(".carousel-next").on("click", function () {
    $carousel.carousel("next");
  });

  $(".carousel-previous").on("click", function () {
    $carousel.carousel("prev");
  });

  $(".download-cv").on("click", function () {
    $("#portfolioModal .modal-body").html(portfolioModalContent);
  });

  triggerImageLoad();
  generateProjectTimelineTemplate();
});

const mobileCheck = function () {
  let check = false;
  (function (a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

const triggerImageLoad = () => {
  $("<img />")
    .attr("src", "./assets/home-background.jpeg")
    .on("load", function () {
      $(this).remove();

      $(".loading").remove();
      $(".carousel-previous").removeClass("d-none");
      $(".carousel-next").removeClass("d-none");
      $("body").css("background", "white");
      $("body").css("overflow-y", "auto");
    });
};

const generateProjectTimelineTemplate = () => {
  for ({
    timeRange,
    description,
    title,
    members,
    technologies,
    responsibilities,
    related,
  } of projectsData) {
    const $timelineItem = $(defaultProjectTimelineTemplate);

    const $timeRange = $timelineItem.find(".timeline__time-range");
    $timeRange.html(timeRange);

    const $description = $timelineItem.find(".timeline__project-description");
    $description.html(description);

    const $title = $timelineItem.find(".timeline__project-title");
    $title.html(title);

    const $members = $timelineItem.find(".timeline__project-members");
    $members.html(members);

    const $technologies = $timelineItem.find(".timeline__project-technologies");
    for (const technology of technologies) {
      $technologies.append(`
        <div class="ml-3 mt-3 text-black-50">
          <span class="fas fa-bolt"></span> ${technology}
        </div>
      `);
    }

    const $responsibilities = $timelineItem.find(
      ".timeline__project-responsibilities"
    );
    for (const responsibility of responsibilities) {
      $responsibilities.append(`
        <div class="ml-3 mt-3 text-black-50">
          <span class="fas fa-bolt"></span> ${responsibility}
        </div>
      `);
    }

    if (related) {
      const { github, demo } = related;
      const $related = $timelineItem.find(".timeline__project-related");
      $related.removeClass("d-none");

      if (github) {
        $related.append(`
          <div class="ml-3 mt-3 text-black-50">
            <span class="fas fa-bolt"></span> Github: <a target="_blank" href="${github}" class="pointer text-primary font-italic">${github}</a>
          </div>
        `);
      }

      if (demo) {
        $related.append(`
          <div class="ml-3 mt-3 text-black-50">
            <span class="fas fa-bolt"></span> Demo: <a target="_blank" href="${demo}" class="pointer text-primary font-italic">${demo}</a>
          </div>
        `);
      }
    }

    $projectTimeline.append($timelineItem);
  }
};

const typed = new Typed("#typed", {
  strings: ["Software Engineer", "Freelancer"],
  typeSpeed: 50,
  backSpeed: 50,
  loop: true,
});

const secondTyped = new Typed("#typed-2", {
  strings: ["Software Engineer", "Freelancer"],
  typeSpeed: 50,
  backSpeed: 50,
  loop: true,
});

function handleNavigateToSpecificCarouselSlide(slideIndex) {
  $carousel.carousel(slideIndex);
}

function handleDownloadPortfolioClick() {
  $window.trigger("resize");
}
